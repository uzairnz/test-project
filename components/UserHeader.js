import React from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';

const UserHeader = ({buttonText, name, dotMenu, time}) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.innerContainer}>
        <TouchableOpacity activeOpacity={0.6} style={styles.btnStyle}>
          <Text style={styles.textStyle}>{buttonText}</Text>
        </TouchableOpacity>
        <Image
          style={styles.imageStyle}
          source={{
            uri: 'https://cdn.pixabay.com/photo/2018/08/21/23/29/forest-3622519__340.jpg',
          }}
        />
        <Text style={styles.textStyle}>{name}</Text>
        <Text style={[styles.textStyle, {color: '#D3D3D380', fontSize: 10, fontWeight: 'normal'}]}>
          {time}
        </Text>
      </View>
      <TouchableOpacity activeOpacity={0.6}>
        <Image
          style={[
            styles.imageStyle,
            {width: 35, height: 35, resizeMode: 'center'},
          ]}
          source={dotMenu}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  btnStyle: {
    width: 96,
    height: 44,
    backgroundColor: '#BE4444',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
  },
  innerContainer: {
    width: '80%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  mainContainer: {
    alignSelf: 'center',
    width: '95%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: '5%',
  },
  imageStyle: {
    width: 50,
    height: 50,
    borderRadius: 30,
  },
});

export default UserHeader;
