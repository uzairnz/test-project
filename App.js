/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  useColorScheme,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import UserHeader from './components/UserHeader';

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    flex: 1,
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View style={{flex: 1, backgroundColor: '#272B35'}}>
        <UserHeader
          buttonText="Funding"
          name="Ralph Edwards"
          dotMenu={require('./assets/menu.png')}
          time="3h ago"
        />
        <View style={{width: '93%', alignSelf: 'center', marginTop: '5%'}}>
          <Text
            style={[
              styles.textStyle,
              {fontSize: 15, color: '#FFFFFF90', textAlign: 'justify'},
            ]}>
            Velit ut ultrices quis viverra eu, ultricies nulla at nec. Ut diam
            venenatis egestas massa vulputate nam. Pretium eros, imperdiet odio
            sit. Natoque quam mi ut leo. Sed ut sit cursus nunc, sit. Magna
            neque vel amet sem vulputate lacus ut.{'\n'}
            {'\n'}
            {'\n'}Diam lacus sed ornare vulputate. Vulputate magna id
            suspendisse aliquam. Sit fames est proin diam morbi purus non. Purus
            donec eu arcu euismod. Volutpat facilisi venenatis phasellus
            maecenas in.
          </Text>
        </View>

        <View style={styles.bottomView}>
          <Text style={styles.textStyle}>
            {'How can you help with this Request?'}
          </Text>
          <View style={styles.inputContainer}>
            <TextInput
              placeholder={'Type here...'}
              placeholderTextColor="#FFFFFF50"
              style={{width: '90%', height: 45, paddingStart: 10}}
            />
            <TouchableOpacity activeOpacity={0.6}>
              <Image
                style={{width: 15, height: 15, resizeMode: 'contain'}}
                source={require('./assets/reply.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  bottomView: {
    width: '100%',
    height: '20%',
    backgroundColor: '#5F749510',
    position: 'absolute',
    bottom: 0,
    padding: 20,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  inputContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#5F749520',
    borderRadius: 25,
    marginTop: '5%',
  },
});

export default App;
